#!/usr/bin/python
"""
    Sample usage:

        python btmon_tdf.py --serial --serial-poll-interval=10 -p --sqlite --sqlite-insert-period=10

"""
import csv
import logging
import optparse
import os
import sys
import threading
import datetime
import traceback
import zipfile

import requests

import util
from device import *
from packet import *
from buffer import *
import constant
import warnings
import platform
import sqlite3


__app__ = 'btmon'
__version__ = '3.1.1'
debug = True

warnings.filterwarnings('ignore', category=DeprecationWarning)  # MySQLdb in 2.6

# External (Optional) Dependencies
try:
    import serial
except ImportError:
    serial = None

try:
    import MySQLdb
except ImportError:
    MySQLdb = None

try:
    from sqlite3 import dbapi2 as sqlite
except ImportError:
    sqlite = None

try:
    import rrdtool
except ImportError:
    rrdtool = None

try:
    import cjson as json

    # XXX: maintain compatibility w/ json module
    setattr(json, 'dumps', json.encode)
    setattr(json, 'loads', json.decode)
except ImportError:
    try:
        import simplejson as json
    except ImportError:
        import json

try:
    import ConfigParser
except ImportError:
    ConfigParser = None

serial_number = ''
# The schema classes encapsulate the structure for saving data to and reading
# data from databases.

class BaseSchema(object):
    def __init__(self):
        pass

    def gettablesql(self, idopt=''):
        return ''

    def db2pkt(self, row):
        return dict()

    def getinsertsql(self, packet):
        return ''

    def _gettype(self, channel):
        if channel.endswith('_aws') or channel.endswith('_pws') or channel.endswith('_ws'):
            return 'bigint'
        if channel[0] == 'p':
            return 'bigint'
        if channel.endswith('volts') or channel[0] == 't' or channel.endswith('_a') or channel.endswith(
                '_amps') or channel.endswith('_w') or channel.endswith('_wh') or channel.endswith(
                '_pwh') or channel.endswith('_nwh') or channel.endswith('_pw') or channel.endswith(
                '_nw') or channel.endswith('_whd'):
            return 'float'
        return 'bigint'


class CountersSchema(BaseSchema):
    def __init__(self):
        super(CountersSchema, self).__init__()
        self.NAME = DB_SCHEMA_COUNTERS

    def gettablesql(self, idopt=''):
        sql = ['(id bigint primary key %s' % idopt, ', time_created bigint', ', serial varchar(10)', ', secs int']
        for c in PACKET_FORMAT.channels(self.NAME):
            sql.append(', %s %s' % (c, self._gettype(c)))
        sql.append(')')
        return ''.join(sql)

    def db2pkt(self, row):
        pkt = dict(row)
        pkt['flag'] = 0  # fake it
        return pkt

    def getinsertsql(self, p):
        labels = ['time_created', 'serial', 'secs']
        values = [str(p['time_created']), "'" + p['serial'] + "'", str(p['secs'])]
        for c in PACKET_FORMAT.channels(self.NAME):
            labels.append(c)
            values.append(str(p[c]))
        sql = ['(', ','.join(labels), ') VALUES (', ','.join(values), ')']
        return ''.join(sql), len(values), p['serial'], p['time_created']


class ECMReadSchema(BaseSchema):
    def __init__(self):
        super(ECMReadSchema, self).__init__()
        self.NAME = DB_SCHEMA_ECMREAD

    def gettablesql(self, idopt=''):
        sql = []
        sql.append('(id bigint primary key %s' % idopt)
        sql.append(', time_created bigint')
        sql.append(', ecm_serial varchar(10)')
        for c in PACKET_FORMAT.channels(self.NAME):
            sql.append(', %s %s' % (c, self._gettype(c)))
        sql.append(')')
        return ''.join(sql)

    def db2pkt(self, row):
        pkt = dict()
        for key in row.keys():
            pktkey = key
            if key.endswith('_amps'):
                pktkey = key.replace('_amps', '_a')
            if key == 'ecm_serial':
                pktkey = 'serial'
            pkt[pktkey] = row[key]
        return pkt

    def getinsertsql(self, p):
        labels = ['time_created', 'ecm_serial']
        values = [str(p['time_created']), "'" + p['serial'] + "'"]
        for c in PACKET_FORMAT.channels(self.NAME):
            pktkey = c.replace('_amps', '_a')
            labels.append(c)
            values.append(str(p[pktkey]))
        sql = []
        sql.append('(')
        sql.append(','.join(labels))
        sql.append(') VALUES (')
        sql.append(','.join(values))
        sql.append(')')
        return ''.join(sql), len(values), p['serial'], p['time_created']


class ECMReadExtSchema(BaseSchema):
    def __init__(self):
        super(ECMReadExtSchema, self).__init__()
        self.NAME = DB_SCHEMA_ECMREADEXT

    def gettablesql(self, idopt=''):
        sql = []
        sql.append('(id bigint primary key %s' % idopt)
        sql.append(', time_created bigint')
        sql.append(', ecm_serial varchar(10)')
        for c in PACKET_FORMAT.channels(self.NAME):
            sql.append(', %s %s' % (c, self._gettype(c)))
        sql.append(')')
        return ''.join(sql)

    def db2pkt(self, row):
        pkt = dict()
        for key in row.keys():
            pktkey = key
            if key.endswith('_whd'):
                pktkey = key.replace('_whd', '_dwh')
            if key == 'ecm_serial':
                pktkey = 'serial'
            pkt[pktkey] = row[key]
        return pkt

    def getinsertsql(self, p):
        labels = ['time_created', 'ecm_serial']
        values = [str(p['time_created']), "'" + p['serial'] + "'"]
        for c in PACKET_FORMAT.channels(self.NAME):
            labels.append(c)
            pktkey = c.replace('_whd', '_dwh')
            values.append(str(p[pktkey]))
        sql = []
        sql.append('(')
        sql.append(','.join(labels))
        sql.append(') VALUES (')
        sql.append(','.join(values))
        sql.append(')')
        return ''.join(sql), len(values), p['serial'], p['time_created']


# The monitor contains the application control logic, tying together the
# communications mechanism with the data collection and data processing.

class Monitor(object):
    def __init__(self, packet_collector, packet_processors):
        self.packet_collector = packet_collector
        self.packet_processors = packet_processors
        dbgmsg('packet format is %s' % PACKET_FORMAT.__class__.__name__)
        dbgmsg('using collector %s' % packet_collector.__class__.__name__)
        dbgmsg('using %d processors:' % len(self.packet_processors))
        for p in self.packet_processors:
            dbgmsg('  %s' % p.__class__.__name__)

    def read(self):
        self.packet_collector.read(PACKET_FORMAT)

    def process(self):
        global serial_number
        dbgmsg('buffer info:')
        for sn in self.packet_collector.packet_buffer.getkeys():
            dbgmsg('  %s: %3d of %3d (%d)' %
                   (sn,
                    self.packet_collector.packet_buffer.size(sn),
                    self.packet_collector.packet_buffer.maxsize,
                    self.packet_collector.packet_buffer.lastmod(sn)))
            serial_number = sn
        for p in self.packet_processors:
            try:
                dbgmsg('processing with %s' % p.__class__.__name__)
                p.process_compiled(self.packet_collector.packet_buffer)
            except Exception, e:
                if not p.handle(e):
                    wrnmsg('Exception in %s: %s' % (p.__class__.__name__, e))
                    if LOGLEVEL >= LOG_DEBUG:
                        traceback.print_exc()

    def run(self):
        try:
            dbgmsg('setup %s' % self.packet_collector.__class__.__name__)
            self.packet_collector.setup()
            for p in self.packet_processors:
                dbgmsg('setup %s' % p.__class__.__name__)
                p.setup()
            threading.Thread(target=main).start()
            while True:
                self.read()
                self.process()

        except KeyboardInterrupt:
            sys.exit(0)
        except Exception, e:
            errmsg(e)
            if LOGLEVEL >= LOG_DEBUG:
                traceback.print_exc()
            sys.exit(1)

        finally:
            for p in self.packet_processors:
                dbgmsg('cleanup %s' % p.__class__.__name__)
                p.cleanup()
            dbgmsg('cleanup %s' % self.packet_collector.__class__.__name__)
            self.packet_collector.cleanup()


# Data Collector classes
#
# all of the collectors are buffered - they contain an array of packets, sorted
# by timestamp and grouped by the serial number of the brultech device.
class BufferedDataCollector(object):
    def __init__(self):
        self.packet_buffer = CompoundBuffer(BUFFER_SIZE)

    # Read the indicated number of bytes.  This should be overridden by derived
    # classes to do the actual reading of bytes.
    def readbytes(self, count):
        return ''

    def setup(self):
        pass

    def cleanup(self):
        pass

    def open(self):
        pass

    def close(self):
        pass

    def read(self, packet_format):
        self._blockingread(packet_format)

    # The default reader invokes the read method of the packet format.  The
    # device type tells us how many packets to expect.
    def _read(self, packet_format):
        maxread = DEVICE_TYPE.numpackets(packet_format.NAME)
        packets = []
        nread = 0
        while nread < maxread:
            nread += 1
            dbgmsg('reading %d of %d packets' % (nread, maxread))
            packets.extend(packet_format.read(self))
        for p in packets:
            p = packet_format.compile(p)
            self.packet_buffer.insert(p['time_created'], p)

    # This is a helper method for derived classes that do blocking reads.
    def _blockingread(self, packet_format):
        dbgmsg('waiting for data from device')
        havedata = False
        nerr = 0
        while (nerr < READ_RETRIES or READ_RETRIES == 0) and not havedata:
            try:
                self.open()
                self._read(packet_format)
                havedata = True
            except ReadError, e:
                dbgmsg('read failed: %s' % e.msg)
            except KeyboardInterrupt, e:
                raise e
            except (EmptyReadError, Exception), e:
                nerr += 1
                dbgmsg('failed read %d of %d' % (nerr, READ_RETRIES))
                errmsg(e)
                if LOGLEVEL >= LOG_DEBUG:
                    traceback.print_exc()
                self.close()
                dbgmsg('waiting %d seconds before retry' % RETRY_WAIT)
                time.sleep(RETRY_WAIT)
        if not havedata:
            raise RetriesExceededError(READ_RETRIES)

    # This is a helper method for derived classes that do polling.  for each
    # device, make a request for a packet, then do a standard read for the
    # response.  catch the exceptions so that they do not cause the processing
    # to abort prematurely.  for each device, permit a limited number of
    # failures before bailing out.
    def _pollingread(self, packet_format, device_list):
        for did in device_list:
            havedata = False
            ntries = 0
            while ntries < POLL_RETRIES and not havedata:
                ntries += 1
                try:
                    dbgmsg('sending request %d to device %s' % (ntries, did))
                    DEVICE_TYPE.requestpacket(self, did)
                    dbgmsg('waiting for data from device %s' % did)
                    self._read(packet_format)
                    havedata = True
                except ReadError, e:
                    dbgmsg('read failed: %s' % e.msg)
                except KeyboardInterrupt, e:
                    raise e
                except Exception, e:
                    dbgmsg('failed request %d of %d for device %s' % (ntries, POLL_RETRIES, did))
                    errmsg(e)
                    if LOGLEVEL >= LOG_DEBUG:
                        traceback.print_exc()
            if not havedata:
                wrnmsg('%d requests failed for device %s' % (POLL_RETRIES, did))


class SerialCollector(BufferedDataCollector):
    def __init__(self, port, rate):
        if not serial:
            print 'Serial Error: serial module could not be imported.'
            sys.exit(1)

        super(SerialCollector, self).__init__()
        self._port = port
        self._baudrate = int(rate)
        self._conn = None
        infmsg('SERIAL: serial port: %s' % self._port)
        infmsg('SERIAL: baud rate: %d' % self._baudrate)

    def readbytes(self, count):
        return self._conn.read(count)

    def open(self):
        if self._conn is not None:
            return
        dbgmsg('SERIAL: opening connection to %s at %d' %
               (self._port, self._baudrate))
        self._conn = serial.Serial(self._port, self._baudrate)

    def close(self):
        if self._conn:
            dbgmsg('SERIAL: closing connection')
            self._conn.close()
            self._conn = None


# the polling collector opens a serial connection, makes a request for data,
# retrieves the data, then closes the connection.  typically this is used with
# the brultech device not in real-time mode.  it supports multiplexed devices.
class PollingSerialCollector(SerialCollector):
    def __init__(self, port, rate, poll_interval):
        super(PollingSerialCollector, self).__init__(port, rate)
        self._poll_interval = int(poll_interval)
        infmsg('SERIAL: poll interval: %d' % self._poll_interval)

    def read(self, packet_format):
        dbgmsg('SERIAL: waiting for %d seconds' % self._poll_interval)
        time.sleep(self._poll_interval)
        try:
            self.open()
            self._pollingread(packet_format, DEVICE_LIST)
        finally:
            self.close()

    def send(self, s):
        dbgmsg('SERIAL: sending %s' % s)
        self._conn.write(s)

    def recv(self, sz=SERIAL_BUFFER_SIZE):
        try:
            dbgmsg('SERIAL: waiting for %d bytes' % sz)
            resp = self._conn.read(sz)
            while len(resp) < sz:
                dbgmsg('SERIAL: waiting for %d bytes' % (sz - len(resp)))
                resp += self._conn.read(sz - len(resp))
            return resp
        except Exception, e:
            dbgmsg('SERIAL: exception while receiving')
            raise e


# the blocking collector opens a serial connection then blocks for any data
# sent over the connection.  typically this is used with the brultech device
# emitting data in real-time mode.  multiplexed devices may suffer from
# collisions.
class BlockingSerialCollector(SerialCollector):
    def __init__(self, port, rate):
        super(BlockingSerialCollector, self).__init__(port, rate)

    def read(self, packet_format):
        self._blockingread(packet_format)


class BaseProcessor(object):
    def __init__(self):
        self.last_processed = dict()
        self.process_period = 1  # in seconds

    def setup(self):
        pass

    def process_compiled(self, packet_buffer):
        now = getgmtime()
        for sn in packet_buffer.getkeys():
            if packet_buffer.size(sn) < 1:
                dbgmsg('buffer is empty for %s' % sn)
                continue
            if not sn in self.last_processed or now >= self.last_processed[sn] + self.process_period:
                if not sn in self.last_processed:
                    ts = packet_buffer.oldest(sn)[0]
                else:
                    ts = self.last_processed[sn]
                data = packet_buffer.newest(sn, ts)
                if len(data) > 1:
                    dbgmsg('%d buffered packets sn:%s' % (len(data), sn))
                    packets = []
                    for a, b in zip(data[0:], data[1:]):
                        try:
                            self.last_processed[sn] = b[0]
                            packets.append(PACKET_FORMAT.calculate(b[1], a[1]))
                        except ZeroDivisionError:
                            infmsg("not enough data in buffer for %s" % sn)
                        except CounterResetError, cre:
                            wrnmsg("counter reset for %s: %s" % (sn, cre.msg))
                    dbgmsg('%d calculated packets sn:%s' % (len(packets), sn))
                    self.process_calculated(packets)
                else:
                    dbgmsg('not enough data for %s' % sn)
                    continue
            else:
                x = self.last_processed[sn] + self.process_period - now
                dbgmsg('waiting %d seconds to process packets for %s' % (x, sn))

    def process_calculated(self, packets):
        pass

    def handle(self, exception):
        return False

    def cleanup(self):
        pass


class PrintProcessor(BaseProcessor):
    def __init__(self):
        super(PrintProcessor, self).__init__()

    def process_calculated(self, packets):
        for p in packets:
            print
            PACKET_FORMAT.printPacket(p)


class DatabaseProcessor(BaseProcessor):
    def __init__(self, table, period):
        super(DatabaseProcessor, self).__init__()
        self.db_table = table
        self.process_period = int(period)
        self.conn = None

    def process_calculated(self, packets):
        for p in packets:
            valsql, nval, sn, ts = SCHEMA.getinsertsql(p)
            sql = []
            sql.append('INSERT INTO %s ' % self.db_table)
            sql.append(valsql)
            dbgmsg('DB: query: %s' % ''.join(sql))
            cursor = self.conn.cursor()
            cursor.execute(''.join(sql))
            cursor.close()
            infmsg('DB: inserted %d values for %s at %s' % (nval, sn, ts))
        self.conn.commit()


class SqliteClient(object):
    def __init__(self, filename, table):
        if not sqlite:
            print 'Sqlite Error: sqlite3 module could not be imported.'
            sys.exit(1)
        if not filename:
            print 'Sqlite Error: no database file specified'
            sys.exit(1)

        self.db_filename = filename
        self.db_table = table
        self.conn = None
        infmsg('SQLITE: file: %s' % self.db_filename)
        infmsg('SQLITE: table: %s' % self.db_table)

    def setup(self):
        self.conn = sqlite.connect(self.db_filename)

    def cleanup(self):
        if self.conn:
            dbgmsg('SQLITE: closing database connection')
            self.conn.close()
            self.conn = None


class SqliteProcessor(DatabaseProcessor, SqliteClient):
    def __init__(self, filename, table, period):
        DatabaseProcessor.__init__(self, table, period)
        SqliteClient.__init__(self, filename, table)
        infmsg('SQLITE: process_period: %d' % self.process_period)

    def setup(self):
        cfg = SqliteConfigurator(self.db_filename, self.db_table)
        cfg.configure()
        SqliteClient.setup(self)

    def cleanup(self):
        SqliteClient.cleanup(self)


class SqliteConfigurator(SqliteClient):
    def __init__(self, filename, table):
        SqliteClient.__init__(self, filename, table)

    def configure(self):
        try:
            self.setup()

            cursor = self.conn.cursor()
            cursor.execute("select name from sqlite_master where type='table' and name='%s'" % self.db_table)
            row = cursor.fetchone()
            cursor.close()

            if row is not None:
                return

            infmsg('SQLITE: creating table %s' % self.db_table)
            cursor = self.conn.cursor()
            cursor.execute('create table %s %s' % (self.db_table, SCHEMA.gettablesql()))
            cursor.close()

            self.conn.commit()

        finally:
            self.cleanup()


def parse_options():
    parser = optparse.OptionParser(version=__version__)

    parser.add_option('-c', '--config-file', dest='configfile', help='read configuration from FILE', metavar='FILE')
    parser.add_option('-p', '--print', action='store_true', dest='print_out', default=False,
                      help='print data to screen')
    parser.add_option('-q', '--quiet', action='store_true', dest='quiet', default=False, help='quiet output')
    parser.add_option('-v', '--verbose', action='store_false', dest='quiet', default=False, help='verbose output')
    parser.add_option('--debug', action='store_true', default=False, help='debug output')
    parser.add_option('--skip-upload', action='store_true', default=False,
                      help='do not upload data but print what would happen')
    parser.add_option('--buffer-size', help='number of packets to keep in cache', metavar='SIZE')
    parser.add_option('--trust-device-clock', action='store_true', default=False,
                      help='use device clock for packet timestamps')
    parser.add_option('--reverse-polarity', default=False, help='reverse polarity on all channels')
    parser.add_option('--device-list', help='comma-separated list of device identifiers', metavar='LIST')

    parser.add_option('--device-type',
                      help='device types include ' + ', '.join(DEVICE_TYPES) + '; default is ' + DEFAULT_DEVICE_TYPE,
                      metavar='TYPE')
    parser.add_option('--packet-format', help='formats include ' + ', '.join(PACKET_FORMATS), metavar='FMT')
    parser.add_option('--db-schema',
                      help='schemas include ' + ', '.join(DB_SCHEMAS) + '; default is ' + DEFAULT_DB_SCHEMA,
                      metavar='SCHEMA')

    group = optparse.OptionGroup(parser, 'database setup options')
    group.add_option('--sqlite-config', action='store_true', default=False, help='configure sqlite database')
    parser.add_option_group(group)

    group = optparse.OptionGroup(parser, 'serial source options')
    group.add_option('--serial', action='store_true', dest='serial_read', default=False, help='read from serial port')
    group.add_option('--serial-port', help='serial port', metavar='PORT')
    group.add_option('--serial-baud', help='serial baud rate', metavar='RATE')
    group.add_option('--serial-poll-interval',
                     help='how often to poll the device for data, 0 indicates block for data; default is 0',
                     metavar='PERIOD')
    parser.add_option_group(group)

    group = optparse.OptionGroup(parser, 'sqlite source options')
    group.add_option('--sqlite-src', action='store_true', dest='sqlite_read', default=False,
                     help='read from sqlite database')
    group.add_option('--sqlite-src-file', help='source database file', metavar='FILE')
    group.add_option('--sqlite-src-table', help='source database table', metavar='TABLE')
    group.add_option('--sqlite-poll-interval', help='how often to poll the database in seconds', metavar='PERIOD')
    parser.add_option_group(group)

    group = optparse.OptionGroup(parser, 'sqlite options')
    group.add_option('--sqlite', action='store_true', dest='sqlite_out', default=False,
                     help='write data to sqlite database')
    group.add_option('--sqlite-file', help='database filename', metavar='FILE')
    group.add_option('--sqlite-table', help='database table', metavar='TABLE')
    group.add_option('--sqlite-insert-period', help='database insert period in seconds', metavar='PERIOD')
    parser.add_option_group(group)

    return parser


def is_run_time():
    config = ConfigParser.ConfigParser()
    config.read('btmon_tdf.cfg')
    day_list = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
    if config.get('general', 'DAY_OF_WEEK') == day_list[datetime.datetime.today().weekday()] \
            and config.get('general', 'UPLOAD_TIME') == datetime.datetime.now().strftime('%H:%M'):
        return True
    else:
        return False


def main():
    """
    - establish 3G connection, connect to the VPN tdf_server, check remote postgresql db
    - export sensor data from the sqlite db to a csv file, compress it, upload it to the remote tdf_server
    :return:
    """
    b_success = True

    while True:
        # Check current data & time to run at given day of week, at a certain time
        if is_run_time() or not b_success or debug:

            print ('Starting upload process')
            if not connect_3g():
                logging.error('Failed to connect to internet through 3G modem.')
            else:
                last_time = get_last_time()
                if last_time is None:
                    logging.error('Failed to get last time, skipping...')

                    # logging.info('Remote postgresql db is already up-to-date')
                    # b_success = True
                    # continue
                else:
                    if export_and_compress() is None:
                        logging.error('Failed to export and compress')
                        b_success = True
                    else:
                        logging.info('Uploading dump.zip...')
                        _result = upload_data()
                        if _result is None:
                            logging.error('Failed to upload')
                        elif _result['code'] == 'OK':
                            logging.info('Success')
                            # Update last upload time to config file
                            config = ConfigParser.ConfigParser()
                            config.read('btmon_tdf.cfg')
                            config.set('general', 'last_time', time.time())
                            config.write(open('btmon_tdf.cfg', 'wb'))
                            os.remove('dump.csv')
                            os.remove('dump.zip')
                            b_success = True
        if debug:
            time.sleep(10)
        else:
            time.sleep(60)


def connect_3g():
    """
        Connect to internet through 3G modem
    :return:
    """
    return True


def get_last_time():
    """
    Check whether remote postgresql db is up-to-date or not
    :return:
    """
    global serial_number
    if serial_number == '':
        return None
    config = ConfigParser.ConfigParser()
    config.read('btmon_tdf.cfg')
    if platform.system() == 'Windows':
        url = 'http://localhost:8585/get_last_time'
    else:
        url = 'http://' + config.get('server', 'host') + ':' + config.get('server', 'port') + '/get_last_time'
    data = {
        'token': config.get('server', 'token'),
        'sn': serial_number
    }
    headers = {'content-type': 'application/json'}
    try:
        r = requests.post(url, data=json.dumps(data), headers=headers)

        print(r.status_code, r.json())
        if r.status_code != 200:
            return None
        return r.json()['last_time']
    except Exception as er:
        logging.error(er)


def export_and_compress():
    """
    Export data from sqlite DB to csv file, and compress
    :return: None if fails, otherwise return compress file name
    """
    # Get last export time from config file
    config = ConfigParser.ConfigParser()
    config.read('btmon_tdf.cfg')
    try:
        last_time = int(config.get('general', 'last_time'))
    except ConfigParser.NoOptionError:
        last_time = 0
    except ConfigParser.NoSectionError:
        last_time = 0
    except ValueError:
        last_time = 0

    conn = sqlite3.connect(constant.DB_FILENAME)
    zf = zipfile.ZipFile('dump.zip', mode='w')
    try:
        cursor = conn.cursor()
        # FIXME: Hard coded the table name, need to be obtained automatically
        cursor.execute('SELECT * FROM gem48ptbin_counters where time_created > {}'.format(last_time))
        rows = cursor.fetchall()
        names = list(map(lambda x: x[0], cursor.description))
        if rows:
            with open('dump.csv', 'wb') as csv_file:
                csv_writer = csv.writer(csv_file, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
                csv_writer.writerow(names)
                csv_writer.writerows(rows)
            # compress csv file
            zf.write('dump.csv')
            zf.close()
            return 'dump.zip'

    except Exception as er:
        logging.exception(er)
        print er
    finally:
        conn.close()
        zf.close()


def upload_data():
    config = ConfigParser.ConfigParser()
    config.read('btmon_tdf.cfg')

    if platform.system() == 'Windows':
        url = 'http://localhost:8585/upload_data'
    else:
        url = 'http://' + config.get('server', 'host') + ':' + config.get('server', 'port') + '/upload_data'

    # url = 'http://' + config.get('server', 'host') + ':' + config.get('server', 'port') + '/upload_data'
    print '=== Uploading dump.zip({})...'.format(os.path.getsize('dump.zip'))
    try:
        fileobj = open('dump.zip', 'rb')
        data = {
            'token': config.get('server', 'token'),
        }
        r = requests.post(url=url, data=data, files={"archive": ("dump.zip", fileobj)})
        if r.status_code == 200:
            return r.json()
        else:
            return None
    except Exception as er:
        logging.error(er)


if __name__ == '__main__':

    log_file_name = 'btmon_tdf.log'
    logging.basicConfig(level=util.LOGLEVEL, filename=log_file_name, format='%(asctime)s: %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')

    if platform.system() == 'Windows':
        export_and_compress()
        result = upload_data()
        print result
        sys.exit(0)

    (options, args) = parse_options().parse_args()

    if options.quiet:
        LOGLEVEL = LOG_ERROR
    if options.debug:
        LOGLEVEL = LOG_DEBUG

    # if there is a configration file, read the parameters from file and set
    # values on the options object.
    if options.configfile:
        if not ConfigParser:
            print 'ConfigParser not loaded, cannot parse config file'
            sys.exit(1)
        config = ConfigParser.ConfigParser()
        try:
            config.read(options.configfile)
            for section in config.sections():  # section names do not matter
                for name, value in config.items(section):
                    if not getattr(options, name):
                        setattr(options, name, cleanvalue(value))
        except AttributeError, e:
            print 'unknown parameter in config file: %s' % e
            sys.exit(1)
        except Exception, e:
            print e
            sys.exit(1)

    infmsg('btmon: %s' % __version__)
    infmsg('python: %s' % sys.version)
    infmsg('platform: %s' % sys.platform)

    if options.skip_upload:
        SKIP_UPLOAD = 1
    if options.trust_device_clock:
        TRUST_DEVICE_CLOCK = 1
    if options.reverse_polarity:
        REVERSE_POLARITY = 1
        infmsg('polarity is reversed')

    if not options.buffer_size:
        options.buffer_size = DEFAULT_BUFFER_SIZE
    BUFFER_SIZE = int(options.buffer_size)

    if not options.device_type:
        options.device_type = DEFAULT_DEVICE_TYPE
    if options.device_type == DEV_ECM1220:
        DEVICE_TYPE = ECM1220Device()
    elif options.device_type == DEV_ECM1240:
        DEVICE_TYPE = ECM1240Device()
    elif options.device_type == DEV_GEM:
        DEVICE_TYPE = GEMDevice()
    else:
        print "Unsupported device type '%s'" % options.device_type
        print 'supported device types include:'
        for dev in DEVICE_TYPES:
            print '  %s' % dev
        sys.exit(1)
    infmsg('device type: %s' % DEVICE_TYPE.NAME)

    if not options.device_list:
        options.device_list = DEVICE_TYPE.DEFAULT_DEVICE_LIST
    try:
        DEVICE_TYPE.check_identifiers(options.device_list)
    except Exception, e:
        print e
        sys.exit(1)
    DEVICE_LIST = DEVICE_TYPE.extract_identifiers(options.device_list)
    infmsg('device list: %s' % DEVICE_LIST)

    if not options.packet_format:
        options.packet_format = DEVICE_TYPE.DEFAULT_PACKET_FORMAT
    if options.packet_format == PF_ECM1240BIN:
        PACKET_FORMAT = ECM1240BinaryPacket()
    elif options.packet_format == PF_ECM1220BIN:
        PACKET_FORMAT = ECM1220BinaryPacket()
    elif options.packet_format == PF_GEM48PTBIN:
        PACKET_FORMAT = GEM48PTBinaryPacket()
    elif options.packet_format == PF_GEM48PBIN:
        PACKET_FORMAT = GEM48PBinaryPacket()
    else:
        print "Unsupported packet format '%s'" % options.packet_format
        print 'supported formats include:'
        for fmt in PACKET_FORMATS:
            print '  %s' % fmt
        sys.exit(1)
    infmsg('packet format: %s' % PACKET_FORMAT.NAME)

    if not options.db_schema:
        options.db_schema = DEFAULT_DB_SCHEMA
    if options.db_schema == DB_SCHEMA_COUNTERS:
        SCHEMA = CountersSchema()
    elif options.db_schema == DB_SCHEMA_ECMREAD:
        SCHEMA = ECMReadSchema()
    elif options.db_schema == DB_SCHEMA_ECMREADEXT:
        SCHEMA = ECMReadExtSchema()
    else:
        print "Unsupported database schema '%s'" % options.db_schema
        print 'supported schemas include:'
        for fmt in DB_SCHEMAS:
            print '  %s' % fmt
        sys.exit(1)
    infmsg('schema: %s' % SCHEMA.NAME)

    # determine the default table name
    # we use a combination of the packet format and schema for the table name
    # in order to minimize conflicts.
    dbtable = '%s_%s' % (PACKET_FORMAT.NAME, SCHEMA.NAME)

    # Database Setup
    # run the database configurator then exit
    if options.sqlite_config:
        db = SqliteConfigurator(options.sqlite_file or DB_FILENAME,
                                options.sqlite_table or dbtable)
        db.configure()
        sys.exit(0)

    # Data Collector setup
    if options.serial_read:
        if options.serial_poll_interval and options.serial_poll_interval > 0:
            col = PollingSerialCollector(options.serial_port or SERIAL_PORT,
                                         options.serial_baud or SERIAL_BAUD,
                                         options.serial_poll_interval)
        else:
            col = BlockingSerialCollector(options.serial_port or SERIAL_PORT,
                                          options.serial_baud or SERIAL_BAUD)
    else:
        print 'Please specify a data source (or \'-h\' for help):'
        print '  --serial      read from serial'
        sys.exit(1)

    # Packet Processor Setup
    if not (options.print_out or options.mysql_out or options.sqlite_out or
            options.rrd_out or options.wattzon_out or options.plotwatt_out or
            options.enersave_out or options.bidgely_out or
            options.peoplepower_out or options.eragy_out or
            options.smartenergygroups_out or options.thingspeak_out or
            options.pachube_out or options.oem_out or
            options.wattvision_out or options.pvo_out):
        print 'Please specify one or more processing options (or \'-h\' for help):'
        print '  --print              print to screen'
        sys.exit(1)

    procs = []

    if options.print_out:
        procs.append(PrintProcessor())

    if options.sqlite_out:
        procs.append(SqliteProcessor
                     (options.sqlite_file or DB_FILENAME,
                      options.sqlite_table or dbtable,
                      options.sqlite_insert_period or DB_INSERT_PERIOD))

    mon = Monitor(col, procs)
    mon.run()

    sys.exit(0)
