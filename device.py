"""
    The device class encapsultes device behaviors such as the default packet
    format and default device addressing mechanism for multiplexed devices.

"""
from constant import *


class BaseDevice(object):
    def __init__(self):
        pass

    @staticmethod
    def extract_identifiers(s):
        return s.split(',')

    def numpackets(self, packetformat):
        return 1


class ECM1220Device(BaseDevice):
    def __init__(self):
        super(ECM1220Device, self).__init__()
        self.NAME = DEV_ECM1220
        self.DEFAULT_DEVICE_LIST = 'fc'
        self.DEFAULT_PACKET_FORMAT = PF_ECM1220BIN
        self.CFM_BYTE = chr(0xfc)

    # ensure that each identifier is one of fc, fd, fe, or ff
    @staticmethod
    def check_identifiers(s):
        for d in s.split(','):
            if not (d == 'fc' or d == 'fd' or d == 'fe' or d == 'ff'):
                raise Exception("bogus device '%s' in list '%s'" % (d, s))

    # each ecm1220 responds to the byte fc (and others?)
    # each ecm1240 responds to one of fc, fd, fe, or ff
    def requestpacket(self, com, ecmid='fc'):
        com.send(chr(int(ecmid, 16)))
        self._confirm(com)
        com.send('SPK')
        self._confirm(com)

    def _confirm(self, com):
        resp = com.recv(1)
        if not resp:
            raise Exception('received empty response')
        if not resp == self.CFM_BYTE:
            raise Exception('wrong response %s, expected %s' %
                            (hex(ord(resp)), hex(ord(self.CFM_BYTE))))


class ECM1240Device(ECM1220Device):
    def __init__(self):
        super(ECM1240Device, self).__init__()
        self.NAME = DEV_ECM1240
        self.DEFAULT_DEVICE_LIST = 'fc'
        self.DEFAULT_PACKET_FORMAT = PF_ECM1240BIN


class GEMDevice(BaseDevice):
    def __init__(self):
        super(GEMDevice, self).__init__()
        self.NAME = DEV_GEM
        self.DEFAULT_DEVICE_LIST = ''
        self.DEFAULT_PACKET_FORMAT = PF_GEM48PTBIN

    # ensure that each identifier is a 5 digit string
    @staticmethod
    def check_identifiers(s):
        for d in s.split(','):
            if len(d) > 0 and not len(d) == 5:
                raise Exception("bogus device '%s' in list '%s'" % (d, s))

    # use the NMBXXXXX identifier for specific serial numbers.  based on
    # section 8 of 'GEM Commands and Packet Format6.doc' from october 2012.
    # multiplexed devices work only with GEM com firmware 1.61 or later.
    @staticmethod
    def requestpacket(com, gemid=''):
        idstr = ''
        if len(gemid) > 0:
            idstr = 'NMB%s' % gemid
        com.send('^^^%sAPISPK' % idstr)

    # when sending data in the ecm1240 binary format, the GEM sends 5 virtual
    # packets, each with a different serial number.
    def numpackets(self, packetformat):
        if packetformat == PF_ECM1240BIN:
            return 5
        return 1
