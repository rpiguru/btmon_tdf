"""
    The packet class encapsultes packet characteristics, including how to read
    packet information.
"""
from util import *
from errors import *


class BasePacket(object):
    def __init__(self):
        self.KEEPALIVE_GEM = 36
        self.KEEPALIVE_ECM = 0
        self.SEC_COUNTER_MAX = 16777216  # 256^3
        self.BYTE4_COUNTER_MAX = 4294967296  # 256^4
        self.BYTE5_COUNTER_MAX = 1099511627776  # 256^5
        self.START_HEADER0 = 254
        self.START_HEADER1 = 255
        self.END_HEADER0 = 255
        self.END_HEADER1 = 254
        self.DATA_BYTES_LENGTH = 0  # must be defined by derived class
        self.PACKET_ID = 0  # must be defined by derived class

    @staticmethod
    def _convert(b):
        return reduce(lambda x, y: x + y[0] * (256 ** y[1]), zip(b, xrange(len(b))), 0)

    def _serialraw(self, packet):
        """extract the serial number from a raw packet"""
        return '0000'

    def _getserial(self, packet):
        """get the serial number from a compiled packet"""
        return self._fmtserial(packet['unit_id'], packet['ser_no'])

    def _fmtserial(self, devid, sn):
        return '%03d%05d' % (devid, sn)

    def _calculate_checksum(self, packet, devid):
        """calculate the packet checksum"""
        checksum = self.START_HEADER0
        checksum += self.START_HEADER1
        checksum += devid
        checksum += sum(packet)
        checksum += self.END_HEADER0
        checksum += self.END_HEADER1
        return checksum & 0xff

    def _checkbyte(self, data, label, evalue):
        if not data:
            raise EmptyReadError("expected %s %s, got nothing" %
                                 (label, hex(evalue)))
        b = ord(data)
        if b != evalue:
            raise ReadError("expected %s %s, got %s" %
                            (label, hex(evalue), hex(b)))

    # for now (oct2012), this is the read method that we use
    def _read1(self, collector, pktlen, pktid):
        data = collector.readbytes(1)
        self._checkbyte(data, 'START_HEADER0', self.START_HEADER0)
        data = collector.readbytes(1)
        self._checkbyte(data, 'START_HEADER1', self.START_HEADER1)

        data = collector.readbytes(1)
        self._checkbyte(data, 'PACKET_ID', pktid)

        packet = ''
        while len(packet) < pktlen:
            data = collector.readbytes(pktlen - len(packet))
            if not data:  # No data left
                raise ReadError('no data after %d bytes' % len(packet))
            packet += data

        if len(packet) < pktlen:
            raise ReadError("incomplete packet: expected %d bytes, got %d" %
                            (pktlen, len(packet)))

        data = collector.readbytes(1)
        self._checkbyte(data, 'END_HEADER0', self.END_HEADER0)
        data = collector.readbytes(1)
        self._checkbyte(data, 'END_HEADER1', self.END_HEADER1)

        pkt = [ord(c) for c in packet]

        # if the checksum is incorrect, ignore the packet
        checksum = self._calculate_checksum(pkt, pktid)
        data = collector.readbytes(1)
        b = ord(data)
        if b != checksum:
            raise ReadError("bad checksum for %s: expected %s, got %s" %
                            (self._serialraw(packet), hex(checksum), hex(b)))

        return [pkt]

    def _calc_secs(self, newpkt, oldpkt):
        ds = float(newpkt['secs'] - oldpkt['secs'])
        if newpkt['secs'] < oldpkt['secs']:
            ds += self.SEC_COUNTER_MAX
            wrnmsg('seconds counter wraparound detected')
        return ds

    # Absolute watt counter increases no matter which way the current goes
    # Polarized watt counter only increase if the current is positive
    # Every polarized count registers as an absolute count
    def _calc_pe(self, tag, ds, ret, prev):
        """calculate power and energy for a 5-byte polarized counter"""

        # FIXME: there is a corner case here when the absolute watt-second
        # counter wraps but the polarized watt-second counter does not, or
        # vice-versa.

        # Detect counter wraparound and deal with it
        daws = ret[tag + '_aws'] - prev[tag + '_aws']
        if ret[tag + '_aws'] < prev[tag + '_aws']:
            daws += self.BYTE5_COUNTER_MAX
            wrnmsg('energy counter wraparound detected for %s' % tag)
        dpws = ret[tag + '_pws'] - prev[tag + '_pws']
        if ret[tag + '_pws'] < prev[tag + '_pws']:
            dpws += self.BYTE5_COUNTER_MAX
            wrnmsg('polarized energy counter wraparound detected for %s' % tag)

        # Calculate average power over the time period
        ret[tag + '_w'] = daws / ds
        pw = dpws / ds
        nw = ret[tag + '_w'] - pw

        if REVERSE_POLARITY:
            ret[tag + '_pw'] = nw
            ret[tag + '_nw'] = pw
        else:
            ret[tag + '_pw'] = pw
            ret[tag + '_nw'] = nw

        # The polarized count goes up only if the sign is positive, so use the
        # value of polarized count to determine the sign of overall watts
        if ret[tag + '_pw'] == 0:
            ret[tag + '_w'] *= -1

        # calculate the watt-hour count and delta
        pwh = ret[tag + '_pws'] / SpH
        nwh = (ret[tag + '_aws'] - ret[tag + '_pws']) / SpH
        if REVERSE_POLARITY:
            ret[tag + '_pwh'] = nwh
            ret[tag + '_nwh'] = pwh
            prev_dwh = (prev[tag + '_aws'] - 2 * prev[tag + '_pws']) / SpH
        else:
            ret[tag + '_pwh'] = pwh
            ret[tag + '_nwh'] = nwh
            prev_dwh = (2 * prev[tag + '_pws'] - prev[tag + '_aws']) / SpH
        ret[tag + '_wh'] = ret[tag + '_pwh'] - ret[tag + '_nwh']
        ret[tag + '_dwh'] = ret[tag + '_wh'] - prev_dwh

    def _calc_pe_4byte(self, tag, ds, ret, prev):
        """calculate power and energy for a 4-byte non-polarized counter"""

        dws = ret[tag + '_ws'] - prev[tag + '_ws']
        if ret[tag + '_ws'] < prev[tag + '_ws']:
            dws += self.BYTE4_COUNTER_MAX
            wrnmsg('energy counter wraparound detected for %s' % tag)

        ret[tag + '_w'] = dws / ds
        ret[tag + '_wh'] = ret[tag + '_ws'] / SpH
        ret[tag + '_dwh'] = dws / SpH

    def channels(self, fltr):
        """return a list of data sources for this packet type"""
        return []

    def read(self, collector):
        """read data from collector, return an array of raw packets"""
        return self._read1(collector, self.DATA_BYTES_LENGTH, self.PACKET_ID)

    def compile(self, packet):
        """convert a raw packet into a compiled packet"""
        pass

    def calculate(self, newer, older):
        """calculate difference between two packets"""
        pass

    def printPacket(self, packet):
        pass


class ECMBinaryPacket(BasePacket):
    def __init__(self):
        BasePacket.__init__(self)

    @staticmethod
    def _getresetcounter(b):
        """extract the reset counter from a byte"""
        return b & 0x07  # 0b00000111 is not recognized by python 2.5

    def _serialraw(self, packet):
        sn1 = ord(packet[26:27])
        sn2 = ord(packet[27:28]) * 256
        id1 = ord(packet[29:30])
        return self._fmtserial(id1, sn1 + sn2)

    def _fmtserial(self, ecmid, sn):
        """ECM serial numbers are 6 characters - unit id then serial"""
        s = '%d' % ecmid
        return s[-1:] + '%05d' % sn


class ECM1220BinaryPacket(ECMBinaryPacket):
    def __init__(self):
        ECMBinaryPacket.__init__(self)
        self.NAME = PF_ECM1220BIN
        self.PACKET_ID = 1
        self.DATA_BYTES_LENGTH = 37  # does not include the start/end headers
        self.NUM_CHAN = 2

    def channels(self, fltr):
        c = []
        if fltr == FILTER_PE_LABELS:
            c = ['ch1', 'ch2']
        elif fltr == FILTER_POWER:
            c = ['ch1_w', 'ch2_w']
        elif fltr == FILTER_ENERGY:
            c = ['ch1_wh', 'ch2_wh']
        elif fltr == FILTER_DB_SCHEMA_COUNTERS:
            c = ['volts', 'ch1_a', 'ch2_a', 'ch1_aws', 'ch2_aws', 'ch1_pws', 'ch2_pws']
        return c

    def compile(self, rpkt):
        """compile a raw packet into a compiled packet"""
        cpkt = dict()

        # Voltage Data (2 bytes)
        cpkt['volts'] = 0.1 * self._convert(rpkt[1::-1])

        # CH1-2 Absolute Watt-Second Counter (5 bytes each)
        cpkt['ch1_aws'] = self._convert(rpkt[2:7])
        cpkt['ch2_aws'] = self._convert(rpkt[7:12])

        # CH1-2 Polarized Watt-Second Counter (5 bytes each)
        cpkt['ch1_pws'] = self._convert(rpkt[12:17])
        cpkt['ch2_pws'] = self._convert(rpkt[17:22])

        # Reserved (4 bytes)

        # Device Serial Number (2 bytes)
        cpkt['ser_no'] = self._convert(rpkt[26:28])

        # Reset and Polarity Information (1 byte)
        cpkt['flag'] = self._convert(rpkt[28:29])

        # Device Information (1 byte)
        cpkt['unit_id'] = self._convert(rpkt[29:30])

        # CH1-2 Current (2 bytes each)
        cpkt['ch1_a'] = 0.01 * self._convert(rpkt[30:32])
        cpkt['ch2_a'] = 0.01 * self._convert(rpkt[32:34])

        # Seconds (3 bytes)
        cpkt['secs'] = self._convert(rpkt[34:37])

        # Use the current time as the timestamp
        cpkt['time_created'] = getgmtime()

        # Add a formatted serial number
        cpkt['serial'] = self._getserial(cpkt)

        return cpkt

    def calculate(self, now, prev):
        """calculate watts and watt-hours from watt-second counters"""

        # if reset counter has changed since last packet, skip the calculation
        c0 = self._getresetcounter(prev['flag'])
        c1 = self._getresetcounter(now['flag'])
        if c1 != c0:
            raise CounterResetError("old: %d new: %d" % (c0, c1))

        ret = now
        ds = self._calc_secs(ret, prev)
        self._calc_pe('ch1', ds, ret, prev)
        self._calc_pe('ch2', ds, ret, prev)

        return ret

    def printPacket(self, p):
        ts = fmttime(time.localtime(p['time_created']))

        print ts + ": Serial: %s" % p['serial']
        print ts + ": Counter: %d" % self._getresetcounter(p['flag'])
        print ts + ": Voltage:           %9.2fV" % p['volts']
        for x in range(1, self.NUM_CHAN + 1):
            print ts + ": Ch%d Current:        %9.2fA" % (x, p['ch%d_a' % x])
        for x in range(1, self.NUM_CHAN + 1):
            print ts + ": Ch%d Watts:          % 13.6fKWh (% 5dW)" % (x, p['ch%d_wh' % x] / 1000, p['ch%d_w' % x])
            print ts + ": Ch%d Positive Watts: % 13.6fKWh (% 5dW)" % (x, p['ch%d_pwh' % x] / 1000, p['ch%d_pw' % x])
            print ts + ": Ch%d Negative Watts: % 13.6fKWh (% 5dW)" % (x, p['ch%d_nwh' % x] / 1000, p['ch%d_nw' % x])


class ECM1240BinaryPacket(ECM1220BinaryPacket):
    def __init__(self):
        ECM1220BinaryPacket.__init__(self)
        self.NAME = PF_ECM1240BIN
        self.PACKET_ID = 3
        self.DATA_BYTES_LENGTH = 59  # does not include the start/end headers
        self.NUM_CHAN = 2
        self.NUM_AUX = 5

    def channels(self, fltr):
        c = []
        if fltr == FILTER_PE_LABELS:
            c = ['ch1', 'ch2', 'aux1', 'aux2', 'aux3', 'aux4', 'aux5']
        elif fltr == FILTER_POWER:
            c = ['ch1_w', 'ch2_w', 'aux1_w', 'aux2_w', 'aux3_w', 'aux4_w', 'aux5_w']
        elif fltr == FILTER_ENERGY:
            c = ['ch1_wh', 'ch2_wh', 'aux1_wh', 'aux2_wh', 'aux3_wh', 'aux4_wh', 'aux5_wh']
        elif fltr == FILTER_DB_SCHEMA_ECMREAD:
            c = ['volts', 'ch1_amps', 'ch2_amps', 'ch1_w', 'ch2_w', 'aux1_w', 'aux2_w', 'aux3_w', 'aux4_w', 'aux5_w']
        elif fltr == FILTER_DB_SCHEMA_ECMREADEXT:
            c = ['volts', 'ch1_a', 'ch2_a', 'ch1_w', 'ch2_w', 'aux1_w', 'aux2_w', 'aux3_w', 'aux4_w', 'aux5_w',
                 'ch1_wh', 'ch2_wh', 'aux1_wh', 'aux2_wh', 'aux3_wh', 'aux4_wh', 'aux5_wh', 'ch1_whd', 'ch2_whd',
                 'aux1_whd', 'aux2_whd', 'aux3_whd', 'aux4_whd', 'aux5_whd', 'ch1_pw', 'ch1_nw', 'ch2_pw', 'ch2_nw',
                 'ch1_pwh', 'ch1_nwh', 'ch2_pwh', 'ch2_nwh']
        elif fltr == FILTER_DB_SCHEMA_COUNTERS:
            c = ['volts', 'ch1_a', 'ch2_a', 'ch1_aws', 'ch2_aws', 'ch1_pws', 'ch2_pws', 'aux1_ws', 'aux2_ws', 'aux3_ws',
                 'aux4_ws', 'aux5_ws', 'aux5_volts']
        return c

    def compile(self, rpkt):
        cpkt = ECM1220BinaryPacket.compile(self, rpkt)

        # AUX1-5 Watt-Second Counter (4 bytes each)
        cpkt['aux1_ws'] = self._convert(rpkt[37:41])
        cpkt['aux2_ws'] = self._convert(rpkt[41:45])
        cpkt['aux3_ws'] = self._convert(rpkt[45:49])
        cpkt['aux4_ws'] = self._convert(rpkt[49:53])
        cpkt['aux5_ws'] = self._convert(rpkt[53:57])

        # DC voltage on AUX5 (2 bytes)
        cpkt['aux5_volts'] = self._convert(rpkt[57:59])

        return cpkt

    def calculate(self, now, prev):
        ret = ECM1220BinaryPacket.calculate(self, now, prev)
        ds = self._calc_secs(ret, prev)
        self._calc_pe_4byte('aux1', ds, ret, prev)
        self._calc_pe_4byte('aux2', ds, ret, prev)
        self._calc_pe_4byte('aux3', ds, ret, prev)
        self._calc_pe_4byte('aux4', ds, ret, prev)
        self._calc_pe_4byte('aux5', ds, ret, prev)

        return ret

    def printPacket(self, p):
        ts = fmttime(time.localtime(p['time_created']))

        print ts + ": Serial: %s" % p['serial']
        print ts + ": Counter: %d" % self._getresetcounter(p['flag'])
        print ts + ": Voltage:            %9.2fV" % p['volts']
        for x in range(1, self.NUM_CHAN + 1):
            print ts + ": Ch%d Current:        %9.2fA" % (x, p['ch%d_a' % x])
        for x in range(1, self.NUM_CHAN + 1):
            print ts + ": Ch%d Watts:          % 13.6fKWh (% 5dW)" % (x, p['ch%d_wh' % x] / 1000, p['ch%d_w' % x])
            print ts + ": Ch%d Positive Watts: % 13.6fKWh (% 5dW)" % (x, p['ch%d_pwh' % x] / 1000, p['ch%d_pw' % x])
            print ts + ": Ch%d Negative Watts: % 13.6fKWh (% 5dW)" % (x, p['ch%d_nwh' % x] / 1000, p['ch%d_nw' % x])
        for x in range(1, self.NUM_AUX + 1):
            print ts + ": Aux%d Watts:         % 13.6fKWh (% 5dW)" % (x, p['aux%d_wh' % x] / 1000, p['aux%d_w' % x])


# GEM binary packet with 48 channels, polarization
class GEM48PBinaryPacket(BasePacket):
    def __init__(self):
        BasePacket.__init__(self)
        self.NAME = PF_GEM48PBIN
        self.PACKET_ID = 5
        self.DATA_BYTES_LENGTH = 613  # does not include the start/end headers
        self.NUM_CHAN = 32  # there are 48 channels, but only 32 usable
        self.NUM_SENSE = 8
        self.NUM_PULSE = 4

    def _serialraw(self, packet):
        sn1 = ord(packet[481:482])
        sn2 = ord(packet[482:483]) * 256
        id1 = ord(packet[485:486])
        return self._fmtserial(id1, sn1 + sn2)

    def _fmtserial(self, gemid, sn):
        """GEM serial numbers are 8 characters - unit id then serial"""
        return "%03d%05d" % (gemid, sn)

    @staticmethod
    def _mktemperature(b):
        # firmware 1.61 and older use this for temperature
        #        t = 0.5 * self._convert(b)

        # firmware later than 1.61 uses this for temperature
        t = 0.5 * ((b[1] & 0x7f) << 8 | b[0])
        if (b[1] >> 7) != 0:
            t = -t

        # check for bogus values that indicate no sensor
        if t > 255:
            t = 0  # should be None, but currently no notion of 'no data'
        return t

    # for now we emit only the first 32 channels.  the additional 16 are not
    # yet accessible.
    def channels(self, fltr):
        c = []
        if fltr == FILTER_PE_LABELS:
            for x in range(1, self.NUM_CHAN + 1):
                c.append('ch%d' % x)
        elif fltr == FILTER_POWER:
            for x in range(1, self.NUM_CHAN + 1):
                c.append('ch%d_w' % x)
        elif fltr == FILTER_ENERGY:
            for x in range(1, self.NUM_CHAN + 1):
                c.append('ch%d_wh' % x)
        # elif fltr == FILTER_PULSE:
        #     for x in range(1, self.NUM_PULSE + 1):
        #         c.append('p%d' % x)
        # elif fltr == FILTER_SENSOR:
        #     for x in range(1, self.NUM_SENSE + 1):
        #         c.append('t%d' % x)
        elif fltr == FILTER_DB_SCHEMA_COUNTERS:
            c = ['volts']
            for x in range(1, self.NUM_CHAN + 1):
                c.append('ch%d_aws' % x)
                # c.append('ch%d_pws' % x)
            # for x in range(1, self.NUM_PULSE + 1):
            #     c.append('p%d' % x)
            # for x in range(1, self.NUM_SENSE + 1):
            #     c.append('t%d' % x)
        return c

    def compile(self, rpkt):
        cpkt = dict()

        # Voltage Data (2 bytes)
        cpkt['volts'] = 0.1 * self._convert(rpkt[1::-1])

        # Absolute/Polarized Watt-Second Counters (5 bytes each)
        for x in range(1, self.NUM_CHAN + 1):
            cpkt['ch%d_aws' % x] = self._convert(rpkt[2 + 5 * (x - 1):2 + 5 * x])
            cpkt['ch%d_pws' % x] = self._convert(rpkt[242 + 5 * (x - 1):242 + 5 * x])

        # Device Serial Number (2 bytes)
        cpkt['ser_no'] = self._convert(rpkt[483:481:-1])

        # Reserved (1 byte)

        # Device Information (1 byte)
        cpkt['unit_id'] = self._convert(rpkt[485:486])

        # Reserved (96 bytes)

        # Seconds (3 bytes)
        cpkt['secs'] = self._convert(rpkt[582:585])

        # Pulse Counters (3 bytes each)
        for x in range(1, self.NUM_PULSE + 1):
            cpkt['p%d' % x] = self._convert(rpkt[585 + 3 * (x - 1):585 + 3 * x])

            # One-Wire Sensors (2 bytes each)
            # the 0.5 multiplier is for DS18B20 sensors
        #        for x in range(1,self.NUM_SENSE+1):
        #            cpkt['t%d' % x] = 0.5 * self._convert(rpkt[597+2*(x-1):597+2*x])
        for x in range(1, self.NUM_SENSE + 1):
            cpkt['t%d' % x] = self._mktemperature(rpkt[597 + 2 * (x - 1):597 + 2 * x])

        # Spare (2 bytes)

        # Add the current time as the timestamp
        cpkt['time_created'] = getgmtime()

        # Add a formatted serial number
        cpkt['serial'] = self._getserial(cpkt)

        return cpkt

    def calculate(self, now, prev):
        """calculate watts and watt-hours from watt-second counters"""

        # FIXME: check the reset flag once that is supported in gem packets
        # until then, if counter drops we assume it is due to a reset
        for x in range(1, self.NUM_CHAN + 1):
            tag = 'ch%d' % x
            c0 = prev[tag + '_aws']
            c1 = now[tag + '_aws']
            if c1 < c0:
                raise CounterResetError("channel: %s old: %d new: %d" %
                                        (tag, c0, c1))

        ret = now
        ds = self._calc_secs(ret, prev)
        for x in range(1, self.NUM_CHAN + 1):
            tag = 'ch%d' % x
            self._calc_pe(tag, ds, ret, prev)

        return ret

    def printPacket(self, p):
        ts = fmttime(time.localtime(p['time_created']))

        print ts + ": Serial: %s" % p['serial']
        print ts + ": Voltage: % 6.2fV" % p['volts']
        for x in range(1, self.NUM_CHAN + 1):
            print ts + ": Ch%02d: % 13.6fKWh (% 5dW)" % (x, p['ch%d_wh' % x] / 1000, p['ch%d_w' % x])
        for x in range(1, self.NUM_PULSE + 1):
            print ts + ": p%d: % 15d" % (x, p['p%d' % x])
        for x in range(1, self.NUM_SENSE + 1):
            print ts + ": t%d: % 15.6f" % (x, p['t%d' % x])


# GEM binary packet with 48 channels, polarization, time stamp
class GEM48PTBinaryPacket(GEM48PBinaryPacket):
    def __init__(self):
        GEM48PBinaryPacket.__init__(self)
        self.NAME = PF_GEM48PTBIN
        self.PACKET_ID = 5
        self.DATA_BYTES_LENGTH = 619  # does not include the start/end headers
        self.USE_PACKET_TIMESTAMP = TRUST_DEVICE_CLOCK  # trust the GEM clock?

    def compile(self, rpkt):
        cpkt = GEM48PBinaryPacket.compile(self, rpkt)

        # Time Stamp (1 byte each)
        cpkt['year'] = self._convert(rpkt[613:614])
        cpkt['mth'] = self._convert(rpkt[614:615])
        cpkt['day'] = self._convert(rpkt[615:616])
        cpkt['hr'] = self._convert(rpkt[616:617])
        cpkt['min'] = self._convert(rpkt[617:618])
        cpkt['sec'] = self._convert(rpkt[618:619])

        # Add the timestamp as epoch
        if self.USE_PACKET_TIMESTAMP:
            tstr = '20%d.%d.%d %d:%d:%d' % (
                cpkt['year'], cpkt['mth'], cpkt['day'],
                cpkt['hr'], cpkt['min'], cpkt['sec'])
            cpkt['time_created'] = int(time.mktime(time.strptime(
                tstr, '%Y.%m.%d %H:%M:%S')))

        return cpkt
