# Add data export from SQLite to CSV, compression, and upload to PostgreSQL DB via VPN over 3G network to existing Python script on Raspberry Pi.
========================================================================

1) Edit script to pull data from the power meter every 2 minutes (this portion of the code is clearly indicated by comments)
    
  We just need to add a parameter named `--serial-poll-interval` like this:
    
            python btmon_tdf.py --serial --serial-poll-interval=120 -p --debug

2) Modify script/OS to execute on startup of the RPi, as it will likely experience regular power outages.
    
- Now let us execute it at startup.
        
        sudo nano /etc/rc.local
   
    Add follow before `exit 0`
        
        (cd /home/pi/btmon_tdf; python btmon_tdf.py --serial --serial-poll-interval=120 -p --sqlite --sqlite-insert-period=120)&
    
3) Modify SQLite schema to remove the "ch#_pws" columns (keep the "_aws"), remove "p1"-"p4" and remove "t1"-"t8". 
    The code to generate these columns should simply be commented out and the lines noted - it may be re-enabled later.
    
    In line 404 of `packet.py`, we can see that channels are appending.
    
    Just comment channel names out.
    
    I just commented line 425, 415~420, 428~431
        
        
4) Add lines to script to allow for configuration of a weekly VPN session (multiple RPi's will run the same script, 
    and use a single VPN login, so they need to be told to connect on a certain day of the week, at a certain time)

5) Add functionality to turn on the 3G cellular connection at specified time, 
    connect to a remote VPN, check a PostgreSQL server with an identical table format to the above SQLite DB 
    for the most recent uploaded data timestamp for that particular meter's "serial," pull the newer data from 
    the RPi's SQLite DB, export the new data to CSV, compress the new data using NanoZIP (position 7 on this list), 
    send the compressed CSV to the remote server over the VPN, decompress the data, upload it to the PostgreSQL 
    database, verify, then terminate the VPN connection and disable the 3G modem connection.
    
      **NOTE: ** If you need to use another type of compress algorithm, please take a look at `export_and_compress` funtion 
      of `btmon_tdf.py` file.
     
      At the end of that function, there is a couple of line(777, 778) to compress `dump.csv` to `dump.zip`.
      
      So you will have to compress `dump.csv` to `dump.zip`
     
-PostgreSQL database needs to be set up to match the SQLite structure ("serial" will be used to identify the separate meters)

-VPN needs to be setup on server, and key made