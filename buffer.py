import bisect

from util import *


class MovingBuffer(object):
    """Maintain fixed-size buffer of data.  Oldest packets are removed."""

    def __init__(self, maxsize):
        self.maxsize = maxsize
        self.packets = []

    def insert(self, timestamp, packet):
        dbgmsg('buffering packet ts:%d sn:%s' % (timestamp, packet['serial']))
        bisect.insort(self.packets, (timestamp, packet))
        if len(self.packets) > self.maxsize:
            del (self.packets[0])

    def newest(self, timestamp):
        """return all packets with timestamp newer than specified timestamp"""
        idx = bisect.bisect(self.packets, (timestamp, {}))
        return self.packets[idx:]

    def oldest(self):
        """return the oldest packet in the buffer"""
        return self.packets[0]

    def size(self):
        return len(self.packets)

    def lastmod(self):
        if len(self.packets) > 0:
            return self.packets[len(self.packets) - 1][0]
        return 0


class CompoundBuffer(object):
    """Variable number of moving buffers, each associated with an ID"""

    def __init__(self, maxsize):
        self.maxsize = maxsize
        self.buffers = dict()
        dbgmsg('buffer size: %d' % self.maxsize)

    def insert(self, timestamp, packet):
        return self.getbuffer(packet['serial']).insert(timestamp, packet)

    def newest(self, ecm_serial, timestamp):
        return self.getbuffer(ecm_serial).newest(timestamp)

    def oldest(self, ecm_serial):
        return self.getbuffer(ecm_serial).oldest()

    def size(self, ecm_serial):
        return self.getbuffer(ecm_serial).size()

    def lastmod(self, ecm_serial):
        return self.getbuffer(ecm_serial).lastmod()

    def getbuffer(self, ecm_serial):
        if ecm_serial not in self.buffers:
            dbgmsg('adding buffer for %s' % ecm_serial)
            self.buffers[ecm_serial] = MovingBuffer(self.maxsize)
        return self.buffers[ecm_serial]

    def getkeys(self):
        return self.buffers.keys()
